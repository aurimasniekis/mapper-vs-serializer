<?php

$loader = require __DIR__.'/vendor/autoload.php';
require_once './helpers.php';

$iterations = 10000;
$object = unserialize(file_get_contents('./data.serialized'));

echo 'Testing Mapper with ' . count($object->getItems()) . ' items and ' .$iterations . ' iterations' .
     PHP_EOL . PHP_EOL;

$stats = [];

for ($i = 0; $i < $iterations; $i++) {
    $startMemory = memory_get_usage(true);
    $startTime = microtime(true);

    json_encode(\Acme\DataMapper::getInstance()->mapObject($object));

    $stats['time'][] = microtime(true) - $startTime;
    $stats['memory'][] = memory_get_usage(true) - $startMemory;
    echo "\rRunning iteration: " . $i;
}

echo "\rFinished Calculating stats" . PHP_EOL . PHP_EOL;

echo sprintfStats('Time:   ', calculateStats($stats['time'], 1000), 'ms');
echo sprintfStats('Memory: ', calculateStats($stats['memory']), 'B ');
