<?php

$loader = require __DIR__.'/vendor/autoload.php';
require_once './helpers.php';

$iterations = 1000;
$object = file_get_contents('./data.json');

echo 'Testing Parser with ' . count((json_decode($object)->items)) . ' items and ' .$iterations . ' iterations' .
     PHP_EOL . PHP_EOL;

$stats = [];

for ($i = 0; $i < $iterations; $i++) {
    $startMemory = memory_get_usage(true);
    $startTime = microtime(true);

    \Acme\DataMapper::getInstance()->parseJson($object);

    $stats['time'][] = microtime(true) - $startTime;
    $stats['memory'][] = memory_get_usage(true) - $startMemory;
    echo "\rRunning iteration: " . $i;
}

echo "\rFinished Calculating stats" . PHP_EOL . PHP_EOL;

echo sprintfStats('Time:   ', calculateStats($stats['time'], 1000), 'ms');
echo sprintfStats('Memory: ', calculateStats($stats['memory']), 'B ');
