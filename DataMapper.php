<?php

namespace Acme;

/**
 * Class DataMapper
 *
 * @package Acme
 * @author Aurimas Niekis <aurimas.niekis@freshcells.de>
 */
class DataMapper
{
    /**
     * @var DataMapper
     */
    protected static $instance;

    /**
     * @return DataMapper
     */
    public static function getInstance()
    {
        if (self::$instance) {
            return self::$instance;
        }

        self::$instance = new self();

        return self::$instance;
    }

    /**
     * @param Items $object
     *
     * @return array
     */
    public function mapObject($object)
    {
        return [
            'items' => $this->mapItemsToArray($object->getItems())
        ];
    }

    /**
     * @param Item[] $items
     *
     * @return array
     */
    public function mapItemsToArray($items)
    {
        $output = [];

        foreach ($items as $item) {
            $output[] = $this->mapItemToArray($item);
        }

        return $output;
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    public function mapItemToArray($item)
    {
        return [
            'id' => $item->getId(),
            'first_name' => $item->getFirstName(),
            'last_name' => $item->getLastName(),
            'company' => $item->getCompany(),
            'email' => $item->getEmail(),
            'country' => $item->getCountry(),
            'ip_address' => $item->getIpAddress(),
            'iabn' => $item->getIabn()
        ];
    }

    public function parseJson($json)
    {
        $json = json_decode($json, true);

        $items = new Items();

        if (array_key_exists('items', $json)) {
            $items->setItems($this->parseItems($json['items']));
        }

        return $items;
    }

    public function parseItems(array $items)
    {
        $output = [];

        foreach ($items as $item) {
            $output[] = $this->parseItem($item);
        }


        return $output;
    }

    public function parseItem(array $item)
    {
        $item = new Item();

        $map = [
            'id' => 'setId',
            'first_name' => 'setFirstName',
            'last_name' => 'setLastName',
            'company' => 'setCompany',
            'email' => 'setEmail',
            'country' => 'setCountry',
            'ip_address' => 'setIpAddress',
            'iabn' => 'setIabn'
        ];

        foreach ($map as $parameter => $method) {
            if (array_key_exists($parameter, $item)) {
                call_user_func(
                    [$item, $method],
                    $item[$parameter]
                );
            }
        }

        return $item;
    }
}
