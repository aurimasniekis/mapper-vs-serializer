<?php

$loader = require __DIR__.'/vendor/autoload.php';

\Doctrine\Common\Annotations\AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

$jsonContent = file_get_contents('./data.json');

$serializer = JMS\Serializer\SerializerBuilder::create()
                                    ->setCacheDir('./cache')
                                    ->setDebug(false)
                                    ->build();

$object = $serializer->deserialize($jsonContent, 'Acme\\Items', 'json');


file_put_contents('./data.serialized', serialize($object));
