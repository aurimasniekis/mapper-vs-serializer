<?php

namespace Acme;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Item
 *
 */
class Item
{
    /**
     * @var int
     *
     * @JMS\SerializedName("id")
     * @JMS\Type("integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @JMS\SerializedName("first_name")
     * @JMS\Type("string")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @JMS\SerializedName("last_name")
     * @JMS\Type("string")
     */
    protected $lastName;

    /**
     * @var string
     *
     * @JMS\SerializedName("company")
     * @JMS\Type("string")
     */
    protected $company;

    /**
     * @var string
     *
     * @JMS\SerializedName("email")
     * @JMS\Type("string")
     */
    protected $email;

    /**
     * @var string
     *
     * @JMS\SerializedName("country")
     * @JMS\Type("string")
     */
    protected $country;

    /**
     * @var string
     *
     * @JMS\SerializedName("id_address")
     * @JMS\Type("string")
     */
    protected $ipAddress;

    /**
     * @var string
     *
     * @JMS\SerializedName("iabn")
     * @JMS\Type("string")
     */
    protected $iabn;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     *
     * @return $this
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     *
     * @return $this
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getIabn()
    {
        return $this->iabn;
    }

    /**
     * @param string $iabn
     *
     * @return $this
     */
    public function setIabn($iabn)
    {
        $this->iabn = $iabn;

        return $this;
    }
}
