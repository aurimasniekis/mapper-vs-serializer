<?php

namespace Acme;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Items
 *
 */
class Items
{
    /**
     * @var Object[]
     *
     * @JMS\Type("array<Acme\Item>")
     * @JMS\SerializedName("items")
     */
    protected $items;

    /**
     * @return Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     *
     * @return $this
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }
}
