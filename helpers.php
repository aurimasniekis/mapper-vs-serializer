<?php

function calculateStats($data, $multiple = 1)
{
    $stats         = [];
    $stats['avg']  = (array_sum($data) / count($data)) * $multiple;
    $stats['min']  = min($data) * $multiple;
    $stats['max']  = max($data) * $multiple;
    $stats['1th']  = getPercentile(1, $data) * $multiple;
    $stats['5th']  = getPercentile(5, $data) * $multiple;
    $stats['25th'] = getPercentile(25, $data) * $multiple;
    $stats['50th'] = getPercentile(50, $data) * $multiple;
    $stats['75th'] = getPercentile(75, $data) * $multiple;
    $stats['90th'] = getPercentile(90, $data) * $multiple;
    $stats['95th'] = getPercentile(95, $data) * $multiple;
    $stats['99th'] = getPercentile(99, $data) * $multiple;

    return $stats;
}

function sprintfStats($title, $stats, $unit)
{

    return sprintf(
               '%s' . PHP_EOL .
               '    Avg: %.2f %s' . PHP_EOL .
               '    Min: %.2f %s' . PHP_EOL .
               '    Max: %.2f %s' . PHP_EOL .
               '    1th: %.2f %s' . PHP_EOL .
               '    5th: %.2f %s' . PHP_EOL .
               '    25th: %.2f %s' . PHP_EOL .
               '    50th: %.2f %s' . PHP_EOL .
               '    75th: %.2f %s' . PHP_EOL .
               '    90th: %.2f %s' . PHP_EOL .
               '    95th: %.2f %s' . PHP_EOL .
               '    99th: %.2f %s',
               $title,
               $stats['avg'],
               $unit,
               $stats['min'],
               $unit,
               $stats['max'],
               $unit,
               $stats['1th'],
               $unit,
               $stats['5th'],
               $unit,
               $stats['25th'],
               $unit,
               $stats['50th'],
               $unit,
               $stats['75th'],
               $unit,
               $stats['90th'],
               $unit,
               $stats['95th'],
               $unit,
               $stats['99th'],
               $unit
           ) . PHP_EOL;
}


function getPercentile($percentile, $data)
{
    sort($data);
    $index = ($percentile / 100) * count($data);
    if (floor($index) == $index) {
        $result = ($data[$index - 1] + $data[$index]) / 2;
    } else {
        $result = $data[floor($index)];
    }

    return $result;
}
